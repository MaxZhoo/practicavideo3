const {findUsers,existUsers} = require("../Controllers")


const servicio= async ({id}) => {

    try {
        
        const existUser = await existUsers({id})
        
        if(existUser.statusCode!==200) throw(existUser.message);
 
        if(!existUser.data) throw("no existe el usuario");

        const findUser = await findUsers({id});

        if(findUser.statusCode!==200) throw(findUser.message);

        if(findUser.data.info.age<18){
            console.log("eres menor de edad");
        }

        console.log(findUser.data);
        
        return {statusCode:200,data:findUser.data}

    } catch (error) {
        
        console.log({step:"Services servicio",error:error.toString()});

        return {statusCode:500,message: error.toString()}
    }
        
}  
        
module.exports = {servicio}