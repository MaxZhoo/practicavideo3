const dotenv = require("dotenv");

dotenv.config();

const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
}

const internalMessage = "No se pudo procesar la solicitud";
module.exports = {redis,internalMessage}