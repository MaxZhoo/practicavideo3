const db = [
    {
        id:1,
        info: {name:"Maxwel",age:10,address:"El agustino",nacionality:"Peruano",editor:"vsc"},
        color: "green",
    },
    {
        id:2,
        info: {name:"Carlos",age:23,address:"Collique",nacionality:"Peruano",editor:"atom"},
        color: "green",
    },
    {
        id:3,
        info: {name:"Fiorella",age:25,address:"Barranco",nacionality:"Peruano",editor:"st"},
        color: "green",
    }
]

module.exports = {db}