const {db} = require("../Models");

const {setTimeout}  =require("timers/promises")

const existUsers = async({id}) =>{

    try {
        const match = db.some(e => e.id === id);

        return {statusCode:200,data:match}

    } catch (error) {

        console.log({step:"Services servicio",error:error.toString()});

        return { statusCode:500,message:error.toString() } ;
    }

}

const findUsers = async({id}) => {

     try {

         const user = db.filter(e => e.id===id)[0];

         await setTimeout(3000)
             
         return {statusCode:200,data:user};

     } catch (error) {
             
         console.log({step:"Services servicio",error:error.toString()});

         return { statusCode:500,message:error.toString() };

     }
}
 
module.exports = {findUsers,existUsers}