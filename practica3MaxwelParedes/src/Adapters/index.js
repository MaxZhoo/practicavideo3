const bull = require("bull")
const {servicio} = require("../Services")
const {redis,internalMessage} = require("../settings")

const queuePrueba =  bull("curso",{redis:{host:redis.host,port:redis.port}});

queuePrueba.process(async(job,done)=>{

    try {

        const {id} = job.data
        
        console.log(id);
        
        let {statusCode,data,message} = await servicio({id});

        if(statusCode!==200) throw(internalMessage)

        done(null, {statusCode,data,message});

    } catch (error) {
        
        done(null,{ message: internalMessage});
    }
 
})

module.exports = {queuePrueba}