const {queuePrueba} = require("../src/Adapters/index")

const main = async()=>{

    const job = await queuePrueba.add({id:80})
 
    const result =  await job.finished();

    console.log(result);
}

main();